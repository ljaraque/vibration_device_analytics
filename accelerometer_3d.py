# -*- coding: utf-8 -*-

#Log Accelerometer Beanair
#imports 
import os
import pandas
import matplotlib.pyplot as plt
import numpy
import scipy
from scipy import signal
import os


def beanair_3d_log_to_dataframe(filename):
    '''
    load data from beanair format 3d log file into a pandas dataframe with
    columns time, x, y and z.
        arguments:
        ----------
        filename : 3d acceleration log file in beanair's format.
    '''
    df_data=pandas.read_csv(filename,sep=';',skiprows=18,header=None)
    df_data.columns=['time','x','y','z']
    df_data.index=date_times
    return df_data

#FUNCTION UNDER DEVELOPMENT
def kalman_filter_acceleration(acceleration_data):
    '''
    use Kalman filter to clean accelerometer data
    Reference: http://pykalman.github.io/
        arguments:
        ----------
        acceleration_data : acceleration measured data.
    '''
    return acceleration_data


PATH = os.path.dirname(os.path.abspath(__file__))
file_name=PATH+"/data/log_fan_100hz_2g"
df_data = beanair_3d_log_to_dataframe(file_name)


t0= "17-11-2015 12:47:22"
date_times=pandas.date_range(t0, periods=30000, freq='10ms')
delta_freq=(1.0/(29999.0))/0.010

#df_data.z=scipy.signal.medfilt(df_data.z,50)
fig, axes = plt.subplots(nrows=3, ncols=2)
plt.subplots_adjust(hspace=0.5, wspace=0.2)
fig.set_size_inches(18.5, 10.5)


width=30000 # sampling_rate[Hz]*time_width[s]
limit_i=0
limit_f=limit_i+width
df_data=df_data.iloc[limit_i:limit_f,:]

print df_data

'''
#Fourier Analysis log
fft_x= 20*numpy.log10((1/30.085)*numpy.abs(numpy.fft.rfft(df_data["x"])))
ts_fft_x=pandas.Series(fft_x)
fft_y= 20*numpy.log10((1/14.9599)*numpy.abs(numpy.fft.rfft(df_data["y"])))
ts_fft_y=pandas.Series(fft_y)
fft_z= 20*numpy.log10(numpy.abs((1/70.83)*numpy.fft.rfft(df_data["z"]-0.985)))
ts_fft_z=pandas.Series(fft_z)
'''

#Fourier Analysis linear
fft_x_im =numpy.fft.rfft(df_data["x"])
fft_x= numpy.abs(fft_x_im)
ts_fft_x=pandas.Series(fft_x)

fft_y_im =numpy.fft.rfft(df_data["y"])
fft_y= numpy.abs(fft_y_im)
ts_fft_y=pandas.Series(fft_y)

fft_z_im = numpy.fft.rfft(df_data["z"])
fft_z = numpy.abs(fft_z_im)
ts_fft_z = pandas.Series(fft_z)

print "ts_fft_x", ts_fft_x

freq_ticks=pandas.Series(x*delta_freq for x in range(0,len(ts_fft_x)))

print "freq_ticks", freq_ticks
print delta_freq

# put frequencies as index for plotting vs freq
ts_fft_x.index=freq_ticks
ts_fft_y.index=freq_ticks
ts_fft_z.index=freq_ticks

#
vel_x_im = fft_y_im / freq_ticks.values /(2*numpy.pi)
vel_x = numpy.abs(vel_x_im)
ts_vel_x=pandas.Series(vel_x)
ts_vel_x.index=freq_ticks


#Plot
df_data.index.name="Time(Date Time)"
a=df_data.drop(['time'],1).plot(lw=1,title="3-Axial Acceleration",ax=axes[0,0],xlim=(pandas.Timestamp('17-11-2015 12:50:22'),pandas.Timestamp('17-11-2015 12:52:22')),ylim=(-1.5,2),grid=True,fontsize=14)
#df_data.drop(['time','x','z'],1).plot(lw=1,title="3-Axial Acceleration",ax=axes[0,1],xlim=(pandas.Timestamp('17-11-2015 12:50:22'),pandas.Timestamp('17-11-2015 12:52:22')),ylim=(-0.1,1.2),grid=True,fontsize=14)
plot_f_x=ts_fft_x.plot(lw=1,title="Frequency Decomposition x Axis",grid=True,ax=axes[0,1],ylim=(0,ts_fft_x[1:].max()),fontsize=14)
plot_f_y=ts_fft_y.plot(lw=1,title="Frequency Decomposition y Axis",grid=True,ax=axes[1,0],ylim=(0,ts_fft_y[1:].max()),fontsize=14)
plot_f_z=ts_fft_z.plot(lw=1,title="Frecuency Decomposition z Axis",grid=True,ax=axes[1,1],ylim=(0,ts_fft_z[1:].max()),fontsize=14)

print "dtype ts_fft_x", ts_fft_x.dtype
print "dtype freq_ticks", freq_ticks.dtype

#vel_x, vel_y, vel_z = acc_to_vel(ts_fft_x,ts_fft_y,ts_fft_z,freq_ticks)
#vel_x.index=freq_ticks
plot_f_vel_x=ts_vel_x.plot(lw=1,title="Vel Frequency Decomposition x Axis",grid=True,ax=axes[2,0],ylim=(0,9.2),fontsize=14)

#plot_f_x.set_xlabel('Frequency(Hz)')
axes[0,0].set_ylabel('Amplitude', fontsize=18)
axes[0,0].set_xlabel('Time', fontsize=18)
axes[0,1].set_ylabel('FFT', fontsize=18)
axes[0,1].set_xlabel('Frequency(Hz)', fontsize=18)
axes[1,0].set_ylabel('FFT', fontsize=18)
axes[1,0].set_xlabel('Frequency(Hz)', fontsize=18)
axes[1,1].set_ylabel('FFT', fontsize=18)
axes[1,1].set_xlabel('Frequency(Hz)', fontsize=18)

axes[2,0].set_ylabel('FFT', fontsize=18)
axes[2,0].set_xlabel('Frequency(Hz)', fontsize=18)

fig.suptitle('Analysis of Bridge Dynamics',fontsize=24)


fig.savefig(PATH+'/results/plot2.png')






