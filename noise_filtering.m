% Load vibration data
clear all;
close all;
clc;

[ix,x,y,z]=textread('data/log_fan_finger_100hz_16g','%s%f%f%f','delimiter',';','headerlines',18);
sub_x=100.*transpose(x(29489:end));
sub_x=transpose(z(1:end));
sub_x_no_mean=sub_x - (ones(1, 30000) * mean(sub_x));

%fid  = fopen('data/log_fan_100hz_2g');
%data = textscan(fid,'%d%f%f%f','Delimiter',';','HeaderLines',18);
%fid = fclose(fid);
%break;


% Signal Detection and Thresholding

T=300;
n=30000;
t2=linspace(-T/2,T/2,n+1);t=t2(1:n);
k=(2*pi/T)*[0:n/2-1 -n/2:-1];
ks=fftshift(k);

%u=sech(t);
u=sub_x_no_mean;
ut=fft(u);

noise=20;
%utn=ut+noise*(randn(1,n)+i*randn(1,n));
utn=ut
un=ifft(utn);
filter=exp(-1/5000*(k-0).^2)+exp(-1/5000*(k+0).^2); %Filter is moved to another frequency # Move this to tune the filter and find a signal
utnf= filter.*utn; %transform of u with noise and filter
unf=ifft(utnf); %Inverse of signal with noise and filtered
utnf2=fft(unf);

subplot(4,1,1),plot(...
t,u,'g',...
t,0*t+0.5,'k:') %detection threshold
axis([-150 150 -1.5 2])


subplot(4,1,2),plot(...
ks,abs(fftshift(utn)),'m',...
ks,fftshift(filter),'b') % Filter
axis([-320 320 0 350])


subplot(4,1,3),plot(...
ks,abs(fftshift(utnf2)),'m')
axis([-320 320 0 350])


subplot(4,1,4),plot(...
t,unf,'g',...
t,0*t+0.5,'k:') %detection threshold
axis([-150 150 -1.5 2])
%t,abs(un),'m',...

%but why the filter there get signal and not in other regions of frequency  if it looks the same shape. It is because the signal there is coherent, not random phase every point. If filter is moved to another freq than no signal is detected.


%subplot(3,1,3),plot(...
%ks,abs(fftshift(utn))/(max(abs(fftshift(utn)))),'m',...
%ks,fftshift(filter),'b',... % Filter
%ks,abs(fftshift(utnf))/(max(abs(fftshift(utnf)))),'m')
%axis([-320 320 0 1])


